<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/post" method="POST">
        @csrf
        <label for="fname">First Name:</label> <br> <br>
        <input type="text" name="first_name"> <br> <br>
        <label for="lname">Last Name:</label> <br> <br>
        <input type="text" name="last_name"> <br> <br>
        <label for="gender">Gender: </label> <br> <br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br> <br>
        <label for="national">Nationality: </label> <br> <br>
        <select name="national">
            <option value="indo">Indonesian</option>
            <option value="malay">Malaysian</option>
            <option value="sg">Singaporean</option>
            <option value="australia">Australian</option>
        </select> <br> <br>
        <label for="language">Language Spoken:</label> <br> <br>
        <input type="checkbox" name="language" id=""> Bahasa Indonesia <br>
        <input type="checkbox" name="language" id=""> English <br>
        <input type="checkbox" name="language" id=""> Other <br> <br>
        <label for="bio">Bio: </label> <br> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br> <br>
        <button type="submit">Sign Up</button>
    </form>  
</body>
</html>