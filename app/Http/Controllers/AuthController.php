<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('form');
    }

    public function post(Request $request) {
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $gender = $request->gender;
        $national = $request->national;
        $language = $request->language;
        $bio = $request->bio;

        return view('welcomed', compact('first_name', 'last_name', 'gender', 'national', 'language', 'bio'));
    }
}
